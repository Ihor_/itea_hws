import { modalHide, modalShow, createInputsForModal, generationID } from "./functions.js";

const btnShowMadal = document.querySelector(".add"),
    btnCloseModal = document.getElementById("close"),
    btnSaveModal = document.getElementById("save"),
    select = document.getElementById("select"),
    formInfo = document.querySelector(".form-info"),
    store = ["Назва продукту", "Вартість продукту", "Посилання на зображення", "Опис продукту", "Ключеві слова (Розділяти комою)"],
    restraunt = ["Назва страви", "Вага страви", "Ингридієнти", "Вартість страви", "Посилання на зображення", "Ключеві слова (Розділяти комою)"],
    hosting = ["Назва відео", "Посилання", "Тривалість відео", "Дата публікації"]
let typeCategory = null;


btnShowMadal.addEventListener("click", modalShow);
btnCloseModal.addEventListener("click", modalHide);

select.addEventListener("change", () => {
    typeCategory = select.value;

    if (select.value === "Магазин") {
        formInfo.innerHTML = ''
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(store));
    } else if (select.value === "Відео хостинг") {
        formInfo.innerHTML = ''
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(hosting));
    } else if (select.value === "Ресторан") {
        formInfo.innerHTML = ''
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(restraunt));
    } else {
        console.error("Жоден з пунктів не валідний.")
        return
    }
})

btnSaveModal.addEventListener("click", () => {
    const [...inputs] = document.querySelectorAll(".form-info input");
    const objStore = {
        id: "",
        status: false,
        productName: "",
        porductPrice: 0,
        productImage: "",
        productDescription: "",
        productQuantity: 0,
        keywords: []
    };
    const objRestraunt = {
        id: "",
        status: false,
        productName: "",
        productWeiht: "",
        ingredients: "",
        price: 0,
        productImageUrl: "",
        keywords: [],
        Weiht: "",
        stopList: false,
        productQuantity: 0,
        ageRestrictions: false,
        like: 0
    };
    const objHosting = {
        id: "",
        videoLink: "",
        videoName: "",
        releaseDate: "",
        videoDuration: "",
    }
        /* ------ Сохранение данных в разделе  "Магазин" ---------------- */
    if (typeCategory === "Магазин") {
        objStore.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "Назва продукту") {
                    objStore.productName = input.value;
                } else if (input.dataset.type === "Вартість продукту") {
                    objStore.porductPrice = parseFloat(input.value);
                } else if (input.dataset.type === "Посилання на зображення") {
                    objStore.productImage = input.value;
                } else if (input.dataset.type === "Опис продукту") {
                    objStore.productDescription = input.value;
                } else if (input.dataset.type === "Ключеві слова (Розділяти комою)") {
                    objStore.keywords.push(...input.value.split(","))
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        objStore.date = new Date();
        if(objStore.productQuantity <= 0){
            objStore.status = false;
        }else{
            objStore.status = true;
        }
        const store = JSON.parse(localStorage.BDStore);
        store.push(objStore);
        localStorage.BDStore = JSON.stringify(store);
    }
    /* -------------- Сохранение данных в разделе "Ресторан" ------------------ */
    else if (typeCategory === "Ресторан") {
        objRestraunt.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "Назва страви") {
                    objRestraunt.productName = input.value;
                } else if (input.dataset.type === "Вага страви") {
                    objRestraunt.productWeiht = input.value;
                } else if (input.dataset.type === "Ингридієнти") {
                    objRestraunt.ingredients = input.value;
                } else if (input.dataset.type === "Вартість страви") {
                    objRestraunt.price = parseFloat(input.value);
                } else if (input.dataset.type === "Посилання на зображення") {
                    objRestraunt.productImageUrl = input.value;
                } else if (input.dataset.type === "Ключеві слова (Розділяти комою)") {
                    objRestraunt.keywords.push(...input.value.split(","))
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        objRestraunt.date = new Date();
        if(objRestraunt.productQuantity <=0){
            objRestraunt.stopList = false;
        }else{
            objRestraunt.stopList = true;
        }
        const restraunt = JSON.parse(localStorage.BDRestraunt);
        restraunt.push(objRestraunt);
        localStorage.BDRestraunt = JSON.stringify(restraunt);
    }
        /* ---------- Сохранение данных в разделе "Видео Хостинг" ---------- */
    else if (typeCategory === "Відео хостинг") {
        objHosting.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "Назва відео") {
                    objHosting.videoName = input.value;
                } else if (input.dataset.type === "Посилання") {
                    objHosting.videoLink = input.value;
                } else if (input.dataset.type === "Тривалість відео") {
                    objHosting.videoDuration = input.value;
                } else if (input.dataset.type === "Дата публікації") {
                    objHosting.releaseDate = input.value;
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }    
        })
        
        const hosting = JSON.parse(localStorage.BDHosting);
        hosting.push(objHosting);
        localStorage.BDHosting = JSON.stringify(hosting);
    }
})







