const tbody = document.querySelector("tbody");
const restraunt = JSON.parse(localStorage.BDRestraunt);


function createElementTable (restraunt) {
   return restraunt.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.productName}</td>
            <td title="При настиску сортувати.">${el.productQuantity}</td>
            <td title="При настиску сортувати.">${el.price} грн.</td>
            <td>&#128397;</td>
            <td>${el.status ? "&#9989;" : "&#10060;" }</td>
            <td>${el.date}</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

tbody.insertAdjacentHTML("beforeend", createElementTable(restraunt))
