const tbody = document.querySelector("tbody");
const hosting = JSON.parse(localStorage.BDHosting);


function createElementTable (hosting) {
   return hosting.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.videoName}</td>
            <td>${el.releaseDate}</td>
            <td><a href="${el.videoLink}">Перейти до відео</a></td>
            <td>&#128397;</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

tbody.insertAdjacentHTML("beforeend", createElementTable(hosting))
